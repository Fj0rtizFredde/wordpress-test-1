<?php
/*
 * Our custom post type
 */
class LexiconWpTest1CustomPost
{
    /*
   * __construct
   */
  public function __construct()
  {
    add_action('init', array($this, 'create_custom_fields'));
  }

  function create_custom_fields()
  {
    $labels = array(
      'name'               => 'Lexicon Custom Fields',
      'add_new'            => 'Add New',
      'add_new_item'       => 'Add More Custom Fields',
      'edit'               => 'Edit',
      'edit_item'          => 'Edit Custom Fields Item',
      'new_item'           => 'Add Custom Fields Item',
      'view'               => 'View',
      'view_item'          => 'View Custom Fields Item',
      'search_items'       => 'Search Lexicon custom fields',
      'not_found'          => 'No custom fields found',
      'not_found_in_trash' => 'No custom fields found in the Trash',
      'parent'             => 'Parent custom fields'
    );

    $args = array(
      'labels'             => $labels,
      'public'             => true,
      'menu_position'      => 5,
      'taxonomies'         => array(''),
      'has_archive'        => true,
      'supports'           => array('title', 'editor', 'comments', 'thumbnail', 'custom-fields')
    );
    
    register_post_type('custom_fields', $args);
  }
}

?>
