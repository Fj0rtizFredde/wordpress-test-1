<?php
/*
 * Widget for our custom post type
 */
class LexiconWpTest1Widget extends WP_Widget
{
  /*
   * __construct
   */
  public function __construct()
  {
    $widget_options = array(
      'classname'   => 'widget_recent_entries',
      'description' => __('Displays custom posts')
    );
    parent::WP_Widget('LexiconWpTest1Widget','lexicon-wp-test1', $widget_options);  
  }

  //Let's create the widget!
  function widget($args, $instance)
  {
    extract($args);
    global $wpdb;
    $number = $instance['number'];
    $args = array( 
      'post_type' => 'custom_fields',
      'posts_per_page' => $number,
      'post_status' => 'publish',
    );
    $query = new WP_Query($args);
    echo $before_widget;
    $this->widget_template($query);
    echo $after_widget;
  }

  //And let's create the widget display!
  function widget_template($data)
  {
    ?>
    <h2 class="widget-title">Recent Custom Posts</h2>
    <ul>
      <?php while ($data->have_posts() ) : $data->the_post();?>
        <li id="post-<?php the_ID(); ?>" >
            <?php the_title(); ?>
            <!-- Display custom posts in the widget -->
            <span class="entry-content"><?php echo get_the_excerpt(); ?></span>
            <?php echo $this->get_first_image(get_the_content()) ?>
        </li>
      <?php endwhile; ?>
    </ul>
    <?php
  }

  //Update widget function!
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['number'] = strip_tags($new_instance['number']);
    return $instance;
  }

  //Let's create the form!
  function form($instance)
  {
    $number = esc_attr($instance['number']);
    ?>
        <p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php echo('Number of posts to show:'); ?></label>
          <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
    <?php
  }
  //Simple function to check if the post has an image!
  function get_first_image($content)
  {
    $first_img = '';
    $output = preg_match_all('/(<img [^>]*>)/', $content, $matches, PREG_PATTERN_ORDER);
    $first_img = $matches [1] [0];
    if(!empty($first_img))
    {
      return $first_img;
    }
  }
}

//Register the widget!
function LexiconWpTest1Widget_init(){
  register_widget('LexiconWpTest1Widget');
}

?>