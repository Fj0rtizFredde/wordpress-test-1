<?php
/*
Plugin Name: Lexicon IT-Konsult: WordPress Test 1
Description: WordPress Test 1
Author: Lexicon IT-Konsult
Version: 1.0
*/

//Setup the custom class:
require_once('class/class-custom-post.php');
$lexicon = new LexiconWpTest1CustomPost(); 

//Setup the widget:
require_once('class/class-widget.php');
add_action('widgets_init', 'LexiconWpTest1Widget_init');